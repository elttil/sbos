#ifndef MSLEEP_H
#define MSLEEP_H
#include <typedefs.h>
void syscall_msleep(u32 ms);
#endif
