#include <typedefs.h>
#include <fs/vfs.h>
#include <defs.h>

void setup_random(void);
void add_random_devices(void);
void get_random(u8* buffer, u64 len);
